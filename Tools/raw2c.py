#!/usr/bin/env python

"""Convert raw binary data to C code."""

import sys

if len(sys.argv) != 3:
    print "Usage: raw2c.py <infile> <outfile>"
    exit(1)

ip = open(sys.argv[1], 'r')
raw = ip.read()
ip.close()

op = open(sys.argv[2], 'w')

l = len(raw)
n = 0

op.write('#define DATA_LEN {:d}\n'.format(l))
op.write('\n')
op.write('const int data_len = DATA_LEN;\n')
op.write('\n')
op.write('const unsigned char data[DATA_LEN] = {')

for b in raw:
    n = n + 1
    if (n % 8 is 1) and (n < l):
        op.write('\n    ')
    op.write('0x{:02X}'.format(ord(b)))
    if n < l:
        op.write(',')
    if (n < l) :
        if (n % 8 is not 0):
            op.write(' ')
op.write('\n};\n')
        
op.close()

