/**
******************************************************************************
* File Name          : main.c
* Date               : 28/03/2015 21:32:34
* Description        : Main program body
******************************************************************************
*
* COPYRIGHT(c) 2015 STMicroelectronics
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of STMicroelectronics nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"
#include "dac.h"
#include "tim.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */

#define TX_ON     (HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, GPIO_PIN_SET))
#define TX_OFF    (HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, GPIO_PIN_RESET))
#define TONE_MODE (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_2) == GPIO_PIN_RESET)

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
volatile unsigned char *audio_data;
volatile int audio_len;

const unsigned char audio_zero[1] = {0x7F};

#define BEEP_CYCLE_LEN 20
const unsigned char beep_cycle[BEEP_CYCLE_LEN] = {
    0x7f, 0x83, 0x88, 0x8b, 0x8e, 0x8f, 0x8e, 0x8b, 0x88, 0x83,
    0x7f, 0x7a, 0x75, 0x72, 0x6f, 0x6f, 0x6f, 0x72, 0x75, 0x7a
};

extern const int callsign_len;
extern const unsigned char *callsign;

extern const int fox_len;
extern const unsigned char *fox;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

void play(const unsigned char *data, int len) {
    audio_data = (unsigned char *)data;
    audio_len  = len;
    while (audio_len > 0) {}
}

void beep(int cycles) {
    int i;
    for (i = 0; i < cycles; i++) {
	play(beep_cycle, BEEP_CYCLE_LEN);
    }
    play(audio_zero, 1);
}

/* USER CODE END 0 */

int main(void)
{

    /* USER CODE BEGIN 1 */
    int i, j;

    audio_data = NULL;
    audio_len = 0;

    /* USER CODE END 1 */

    /* MCU Configuration----------------------------------------------------------*/

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* Configure the system clock */
    SystemClock_Config();

    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_DAC1_Init();
    MX_TIM6_Init();

    /* USER CODE BEGIN 2 */

    HAL_TIM_Base_Start_IT(&htim6);


    if (TONE_MODE) {
	// Switch is in TONE position:
	// Transmit intermittently
	while(1) {
	    TX_ON;
	    HAL_Delay(100);
	    beep(300);
	    HAL_Delay(100);
	    TX_OFF;
	    HAL_Delay(3000);
	}
    } else {
	// Switch is in VOICE position:
	// Transmit continuously
	TX_ON;
	while(1) {
	    HAL_Delay(1000);
	    play((const unsigned char *)&callsign, callsign_len);
	    HAL_Delay(1000);
	    for (i = 0; i < 15; i++) {
		play((const unsigned char *)&fox, fox_len);
		HAL_Delay(1000);
		for (j = 0; j < 10; j++) {
		    beep(300);
		    HAL_Delay(3000);
		}
	    }
	}
    }
    /* USER CODE END 2 */
    
    /* USER CODE BEGIN 3 */
    /* Infinite loop */
    while (1)
    {

    }
    /* USER CODE END 3 */

}


/** System Clock Configuration
 */
void SystemClock_Config(void)
{

    RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_ClkInitTypeDef RCC_ClkInitStruct;

    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = 16;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
    HAL_RCC_OscConfig(&RCC_OscInitStruct);

    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0);

    __SYSCFG_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

void _init(void) {}

void HAL_DAC_ConvCpltCallbackCh1(DAC_HandleTypeDef* hdac)
{
}

/* USER CODE END 4 */

#ifdef USE_FULL_ASSERT

/**
 * @brief Reports the name of the source file and the source line number
 * where the assert_param error has occurred.
 * @param file: pointer to the source file name
 * @param line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */

}

#endif

/**
 * @}
 */ 

/**
 * @}
 */ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
